var express = require('express')();
var http = require('http').Server(express);
var io = require('socket.io')(http);
var admin = require("firebase-admin");

var firebaseCredential = require(__dirname + '/private/serviceCredential.json')


admin.initializeApp({
  credential: admin.credential.cert(firebaseCredential),
  databaseURL: "https://beastchat-a6bdc.firebaseio.com"
});

var accountRequests = require('./firebase/account-services');

accountRequests.userAccountRequests(io);

http.listen(3000, ()=>{
  console.log('Server is listening on port 3000')
});
